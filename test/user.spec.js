
describe('The user module', () => {
  beforeEach(module('user'));
  
  var UserController, $scope;
  beforeEach(inject(($controller, $rootScope) => {
    $scope = $rootScope.$new();
    UserController = $controller('UserController', {$scope: $scope});
  }));
  
  it('should initialize the user name', () => {
    expect($scope.user.first_name).toEqual('Barry');
    expect($scope.user.last_name).toEqual('Allen');
  });
  
  it('should return the full name', () => {
    var user = {
      first_name: 'Oliver',
      last_name: 'Queen'
    };
    expect($scope.fullName(user)).toEqual('Oliver Queen');
  });
});
  