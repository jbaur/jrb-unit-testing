// Karma configuration

module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],
    files: [
      // "app/lib/jquery/jquery.js",
      "www/lib/angular/angular.js",
      "www/lib/angular-route/angular-route.js",
      "www/lib/angular-mocks/angular-mocks.js",
      'www/app/**/*.js',
      'test/**/*.js'
    ],
    exclude: [
    ],
    reporters: ['progress'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    captureTimeout: 60000,
    singleRun: false
  });
};
